import logging
import redis


class Config(object):
    """项目配置信息类"""

    # 设置SECRET_KEY
    SECRET_KEY = 'NMIC447SF1oPJ3sx/F7jDlHKJsc387BfdBEIwJDgRjMAUXwcN1VYxSJoIJGrQaJ+'

    # 数据库的配置信息
    SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@127.0.0.1:3306/infomation'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # redis配置
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379

    # session存储的配置信息
    SESSION_TYPE = "redis"  # 指定session保存到redis中
    SESSION_USE_SIGNER = True  # 让cookie中的session_id被加密签名处理
    SESSION_REDIS = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT)  # 使用redis的实例
    PERMANENT_SESSION_LIFETIME = 3600*24*2  # session的有效期，单位是秒


class DevelopementConfig(Config):
    """开发模式下的配置类"""
    # 开启调试模式
    DEBUG = True
    # 开发环境中的日志等级
    LOG_LEVEL = logging.DEBUG


class ProductionConfig(Config):
    """生产模式下的配置类"""
    # 配置生产环境中使用的配置类
    SQLALCHEMY_DATABASE_URI = 'mysql://root:mysql@127.0.0.1:3306/info'

    # 生产环境中的日志等级
    LOG_LEVEL = logging.WARNING


# 定义配置字典
config_dict = {
    'development': DevelopementConfig,
    'production': ProductionConfig
}
